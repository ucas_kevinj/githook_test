# -*- coding: utf-8 -*-
"""
Created on Fri Feb 22 08:52:56 2019

@author: kevinj
"""
import unittest
from fizzbuzz import fizzbuzz

class TestFizzBuzz(unittest.TestCase):
    """Code to check whether fizzbuzz works"""
    def test_fizzbuzz_correct_values(self):
        """Checks whether errors are produced by incorrect values"""
        self.assertRaises(ValueError, fizzbuzz, -2)
        self.assertRaises(TypeError, fizzbuzz, 2.2)
    def test_fizzbuzz_same_value(self):
        """Checks if non multiples of 3 or 5 have the same input and output"""
        self.assertEqual(fizzbuzz(1), 1)
        self.assertEqual(fizzbuzz(2), 2)
    def test_fizzbuzz_fizz_values(self):
        """Checks if multiples of 3 output fizz"""
        self.assertEqual(fizzbuzz(3), "fizz")
        self.assertEqual(fizzbuzz(6), "fizz")
    def test_fizzbuzz_buzz_values(self):
        """Checks if multiples of 5 output buzz"""
        self.assertEqual(fizzbuzz(5), "buzz")
        self.assertEqual(fizzbuzz(10), "buzz")
    def test_fizzbuzz_fizzbuzz_values(self):
        """Checks if multiples of 3 and 5 output fizzbuzz"""
        self.assertEqual(fizzbuzz(15), "fizzbuzz")
