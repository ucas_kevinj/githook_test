# -*- coding: utf-8 -*-
"""
Created on Fri Feb 22 08:52:54 2019

@author: kevinj 

"""

def mod(in_value, dividor, word):
    """returns the required word for a given multiple"""
    if in_value % dividor == 0:
        return word
    return ""

def fizzbuzz(in_value):
    """returns the correct value for that number in a game of fizzbuzz"""
    out = ""
    if in_value < 1:
        raise ValueError("input must be positive")
    if not(isinstance(in_value, int)):
        raise TypeError("input must be an integer")
    for i, j in zip([3, 5], ["fizz", "buzz"]):
        out += mod(in_value, i, j)
    if out == "":
        out = in_value
    return out
